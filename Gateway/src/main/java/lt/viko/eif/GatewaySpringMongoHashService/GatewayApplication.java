package lt.viko.eif.GatewaySpringMongoHashService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Program that implements the hash service the client side
 *
 * @author Eimantas Ilkevičius, Ivan Stanul
 */
@SpringBootApplication
public class GatewayApplication extends SpringBootServletInitializer {

  /**
   * SpringApplicationBuilder method used to create wars files.
   */
  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(GatewayApplication.class);
  }

  /**
   * Main method that runs the Application
   *
   * @param args string of arguments
   */
  public static void main(String[] args) {
    SpringApplication.run(GatewayApplication.class, args);
  }

}
