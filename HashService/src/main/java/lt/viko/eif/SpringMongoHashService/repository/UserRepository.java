package lt.viko.eif.SpringMongoHashService.repository;

import lt.viko.eif.SpringMongoHashService.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * UserRepository interface that extends mongo repository for usage of mongo db commands
 */
public interface UserRepository extends MongoRepository<User, String> {

}
