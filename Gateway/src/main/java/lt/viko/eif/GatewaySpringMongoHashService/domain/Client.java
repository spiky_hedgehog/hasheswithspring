package lt.viko.eif.GatewaySpringMongoHashService.domain;

import org.springframework.hateoas.ResourceSupport;

/**
 * POJO class Client
 */
public class Client extends ResourceSupport {

  private String firstName;
  private String lastName;
  private String clientID;
  private String clientPassword;
  private String originalText;

  /**
   * Empty constructor for Client class
   */
  public Client() {
  }

  /**
   * Constructor of the Client class
   *
   * @param firstName first name of the client
   * @param lastName last name of client
   * @param clientID ID of the client
   * @param clientPassword password of the client
   * @param originalText text of the user to be sent out for hashing
   */
  public Client(String firstName, String lastName, String clientID, String clientPassword,
      String originalText) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.clientID = clientID;
    this.clientPassword = clientPassword;
    this.originalText = originalText;
  }


  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getClientID() {
    return clientID;
  }

  public void setClientID(String clientID) {
    this.clientID = clientID;
  }

  public String getClientPassword() {
    return clientPassword;
  }

  public void setClientPassword(String clientPassword) {
    this.clientPassword = clientPassword;
  }

  public String getOriginalText() {
    return originalText;
  }

  public void setOriginalText(String originalText) {
    this.originalText = originalText;
  }


}
