package lt.viko.eif.SpringMongoHashService.domain;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Class for testing password generation
 */
public class PasswordGeneratorTest {

  /**
   * PasswordGenerator instance
   */
  private PasswordGenerator instance = new PasswordGenerator();

  /**
   * PasswordGenerator test
   */
  @Test
  public void PasswordGenerator() {
    int length = 16;
    assertNotNull(instance.NewPassword());
    assertEquals(instance.NewPassword().length(), length);
    assertNotEquals(instance.NewPassword(), instance.NewPassword());
  }

}