package lt.viko.eif.SpringMongoHashService.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/**
 * Class for testing HashPairMatcher class
 */
public class HashPairMatcherTest {

  /**
   * HashPairMatcher instance
   */
  private HashPairMatcher instance = new HashPairMatcher();

  /**
   * Method for testing region map for Asia
   */
  @Test
  public void RegionMapAsia() {
    String wantedResult = "D0778FDB3DBB60A0AA356627F28FC0F1";
    assertNotNull(instance.generateHash("Kitten", "Asia"));
    assertEquals(wantedResult, instance.generateHash("Kitten", "Asia"));
  }

  /**
   * Method for testing region map for Europe
   */
  @Test
  public void RegionMapEurope() {
    String wantedResult = "3def1ab14172cc9f0bcc4fca2b51b2c8a7864474855957b0e7ff3dcec9803977c88610c7d8f59d6721855cf9fdbb8a18";
    assertNotNull(instance.generateHash("Kitten", "Europe"));
    assertEquals(wantedResult.toUpperCase(), instance.generateHash("Kitten", "Europe"));
  }

  /**
   * Method for testing region map for Australia
   */
  @Test
  public void RegionMapAustralia() {
    String wantedResult = "12D6736F4D2331EF6EE76A614F756BE4DEFFC4BD1329BCB5BF1B56FD1771881C";
    assertNotNull(instance.generateHash("Kitten", "Australia"));
    assertEquals(wantedResult.toUpperCase(), instance.generateHash("Kitten", "Australia"));
  }

}