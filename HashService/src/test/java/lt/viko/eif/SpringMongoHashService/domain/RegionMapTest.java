package lt.viko.eif.SpringMongoHashService.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/**
 * Class for testing RegionMap Class
 */
public class RegionMapTest {

  /**
   * RegionMap Instance
   */
  private RegionMap instance = new RegionMap();

  /**
   * RegionMap test for Asia
   */
  @Test
  public void RegionMapAsia() {
    String countryCode = "YE";
    String wantedContinent = "Asia";
    assertNotNull(instance.getRegion(countryCode));
    assertEquals(wantedContinent, instance.getRegion(countryCode));
  }

  /**
   * RegionMap test for Europe
   */
  @Test
  public void RegionMapEurope() {
    String countryCode = "UA";
    String wantedContinent = "Europe";
    assertNotNull(instance.getRegion(countryCode));
    assertEquals(wantedContinent, instance.getRegion(countryCode));
  }

  /**
   * RegionMap test for Australia
   */
  @Test
  public void RegionMapAustralia() {
    String countryCode = "TV";
    String wantedContinent = "Australia";
    assertNotNull(instance.getRegion(countryCode));
    assertEquals(wantedContinent, instance.getRegion(countryCode));
  }

  /*  *//**
   * RegionMap test for null value
   *//*
  @Test
  public void RegionMapNullTest() {
    String countryCode = null;
    String wantedContinent = "Europe";
    assertEquals(wantedContinent, instance.getRegion(countryCode));
  }*/

}