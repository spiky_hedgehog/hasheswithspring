package lt.viko.eif.GatewaySpringMongoHashService.domain;

import org.springframework.hateoas.ResourceSupport;

/**
 * Pojo class hash
 */
public class Hash extends ResourceSupport {

  private String Hash;

  public Hash() {
  }

  /**
   * Constructor for class hash
   *
   * @param hash hash that we got from the other application
   */
  public Hash(String hash) {
    Hash = hash;
  }


  public String getHash() {
    return Hash;
  }

  public void setHash(String hash) {
    Hash = hash;
  }
}
