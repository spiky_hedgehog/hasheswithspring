package lt.viko.eif.SpringMongoHashService.domain;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Class used for creating a random password
 */
class PasswordGenerator {

  private static final Random RANDOM = new SecureRandom();
  private static final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

  /**
   * Class used for creating a random PasswordGenerator
   *
   * @return Returns newly generated password
   */
  String NewPassword() {
    int passwordLength = 16;
    return generatePassword(passwordLength);
  }

  /**
   * Method used for generating the password
   *
   * @param length length of the password
   * @return and string of random chars and letters that will be used as a password
   */
  private static String generatePassword(int length) {
    StringBuilder returnValue = new StringBuilder(length);
    for (int i = 0; i < length; i++) {
      returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
    }
    return new String(returnValue);
  }
}
