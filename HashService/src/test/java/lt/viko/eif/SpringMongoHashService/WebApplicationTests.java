package lt.viko.eif.SpringMongoHashService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Class for server side application test
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WebApplicationTests {

  /**
   * Test for context Load
   */
  @Test
  public void contextLoads() {
  }

}
