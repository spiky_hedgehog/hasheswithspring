package lt.viko.eif.SpringMongoHashService.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/**
 * Class for testing hash functions
 */
public class HashFunctionsTest {

  /**
   * Instance of class HashFunctions
   */
  private HashFunctions instance = new HashFunctions();

  /**
   * Test of md5 hash method
   */
  @Test
  public void md5() {
    String word = "Kitten";
    String wantedResult = "9B16055070B8269A9DC504251587D0A3";
    assertNotNull(instance.md5(word));
    assertEquals(wantedResult, instance.md5(word));
  }

  /**
   * Test of sha256 hash method
   */
  @Test
  public void sha256() {
    String word = "Kitten";
    String wantedResult = "12D6736F4D2331EF6EE76A614F756BE4DEFFC4BD1329BCB5BF1B56FD1771881C";
    assertNotNull(instance.sha256(word));
    assertEquals(wantedResult, instance.sha256(word));
  }

  /**
   * Test of sha384 hash method
   */
  @Test
  public void sha384() {
    String word = "Kitten";
    String wantedResult = "3def1ab14172cc9f0bcc4fca2b51b2c8a7864474855957b0e7ff3dcec9803977c88610c7d8f59d6721855cf9fdbb8a18";
    assertNotNull(instance.sha384(word));
    assertEquals(wantedResult.toUpperCase(), instance.sha384(word));
  }

  /**
   * Test of sha512 hash method
   */
  @Test
  public void sha512() {
    String word = "Kitten";
    String wantedResult = "4c1845006aed9cfbcb8b9df8f91b1d80200ea7f1f05ed47fad8511414b1361042b5e725d113a6e6f97ef71e5334fba2f534caf317bbdb86dd51a28957b23ae7f";
    assertNotNull(instance.sha512(word));
    assertEquals(wantedResult.toUpperCase(), instance.sha512(word));
  }

  /**
   * Test of murmur3_32 hash method
   */
  @Test
  public void murmur3_32() {
    String word = "Kitten";
    String wantedResult = "45D7B93B";
    assertNotNull(instance.murmur3_32(word));
    assertEquals(wantedResult.toUpperCase(), instance.murmur3_32(word));
  }

  /**
   * Test of murmur3_128 hash method
   */
  @Test
  public void murmur3_128() {
    String word = "Kitten";
    String wantedResult = "D0778FDB3DBB60A0AA356627F28FC0F1";
    assertNotNull(instance.murmur3_128(word));
    assertEquals(wantedResult.toUpperCase(), instance.murmur3_128(word));
  }

  /**
   * Test of hashing method with salt
   */
  @Test
  public void sipHash24() {
    String word = "Kitten";
    String wantedResult = "2F2A63A1C1C45753";
    assertNotNull(instance.sipHash24(word));
    assertEquals(wantedResult, instance.sipHash24(word));
  }

  /**
   * Test of sipHash24 hash method
   */
  @Test
  public void saltedHashWithCustomSalt() {
    String word = "Kitten";
    String salt = "Fluffy";
    String wantedResult = "8b9fa960198ce668143bd2955e2a5d53";
    assertNotNull(instance.saltedHashWithCustomSalt(word, salt));
    assertEquals(wantedResult, instance.saltedHashWithCustomSalt(word, salt));
  }

}