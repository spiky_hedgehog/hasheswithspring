package lt.viko.eif.SpringMongoHashService.domain;

import org.springframework.web.client.RestTemplate;

/**
 * Class used to determine region of a user
 */
public class IdentificationCreator {

  /**
   * Method used for getting clients country code
   *
   * @param IPAddress clients IP address
   * @return returns the country code in string format.
   */
  private String getClientCountryCode(String IPAddress) {
    RestTemplate restTemplate = new RestTemplate();
    return restTemplate.getForObject(IPAddress, String.class);
  }

  /**
   * Method for getting the continent of the client
   *
   * @param IPAddress clients IP address
   * @return returns the string value of clients continent
   */
  public String getClientRegion(String IPAddress) {
    try {
      String codeCodeShort = getClientCountryCode(IPAddress).substring(16, 18);
      return new RegionMap().getRegion(codeCodeShort);
    } catch (Exception e) {
      e.printStackTrace();
      return "Wont work on local machine, since need public IP";
    }
  }

}
