package lt.viko.eif.SpringMongoHashService.domain;

import java.time.Instant;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.hateoas.ResourceSupport;

/**
 * POJO class User
 */
@Document
public class User extends ResourceSupport {

  @Id
  private String userId;
  @CreatedDate
  @DateTimeFormat(iso = ISO.DATE_TIME)
  private Instant createdDate = Instant.now();
  private String firstName;
  private String lastName;
  private String text;
  private String password = new PasswordGenerator().NewPassword();
  private String linkToAPI;


  /**
   * Empty constructor for User class
   */
  public User() {
  }


  /**
   * Constructor of the User class
   *
   * @param userId ID of a user
   * @param createdDate date of the creation of the user
   * @param firstName first name of the user
   * @param lastName last name of the user
   * @param password password of a user
   * @param text text that he wants to hash
   * @param linkToAPI IP of a user
   */
  public User(String userId, Instant createdDate, String firstName, String lastName,
      String text, String password, String linkToAPI) {
    this.userId = userId;
    this.createdDate = createdDate;
    this.firstName = firstName;
    this.lastName = lastName;
    this.text = text;
    this.password = password;
    this.linkToAPI = linkToAPI;
  }


  /**
   * Overridden method to string
   */
  @Override
  public String toString() {
    return "User{" +
        "userId='" + userId + '\'' +
        "dateCreated='" + createdDate + '\'' +
        ", firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", text='" + text + '\'' +
        ", password='" + password + '\'' +
        ", linkToAPI" + linkToAPI +
        '}';
  }

  public Instant getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Instant createdDate) {
    this.createdDate = createdDate;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getLinkToAPI() {
    return linkToAPI;
  }

  public void setLinkToAPI(String linkToAPI) {
    this.linkToAPI = linkToAPI;
  }
}
