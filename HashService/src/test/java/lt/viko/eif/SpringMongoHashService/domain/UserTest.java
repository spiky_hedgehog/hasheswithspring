package lt.viko.eif.SpringMongoHashService.domain;

import static org.junit.Assert.*;

import java.time.Instant;
import org.junit.Test;

/**
 * Class containing methods testing User class getters and setters
 */
public class UserTest {

  /**
   * Test for user id getter and setter
   */
  @Test
  public void testUserIdGetterAndSetter() {
    final User bobId = new User();
    bobId.setUserId("Number1");
    String bobIdSaved = bobId.getUserId();
    assertNotNull(bobId.getUserId());
    assertEquals(bobIdSaved, "Number1");
  }

  /**
   * Test for user CreatedDate getter and setter
   */
  @Test
  public void testCreatedDateGetterAndSetter() {
    final User bobbyCreateDate = new User();
    Instant createdDate = Instant.now();
    bobbyCreateDate.setCreatedDate(createdDate);
    String bobbyCreateDateSaved = bobbyCreateDate.getCreatedDate().toString();
    assertNotNull(bobbyCreateDate.getCreatedDate());
    assertEquals(createdDate.toString(), bobbyCreateDateSaved);
  }

  /**
   * Test for user FirstName getter and setter
   */
  @Test
  public void testFirstNameGetterAndSetter() {
    final User raccoonFirstName = new User();
    raccoonFirstName.setFirstName("Stinky");
    String raccoonFirstNameSaved = raccoonFirstName.getFirstName();
    assertNotNull(raccoonFirstName.getFirstName());
    assertEquals(raccoonFirstNameSaved, "Stinky");
  }

  /**
   * Test for user LastName getter and setter
   */
  @Test
  public void testLastNameGetterAndSetter() {
    final User stacyLastName = new User();
    stacyLastName.setLastName("Gates");
    String stacyLastNameSaved = stacyLastName.getLastName();
    assertNotNull(stacyLastName.getLastName());
    assertEquals(stacyLastNameSaved, "Gates");
  }

  /**
   * Test for user Text getter and setter
   */
  @Test
  public void testTextGetterAndSetter() {
    final User johanText = new User();
    johanText.setText("My Text");
    String johanTextSaved = johanText.getText();
    assertNotNull(johanText.getText());
    assertEquals(johanTextSaved, "My Text");
  }

  /**
   * Test for user Password getter and setter
   */
  @Test
  public void testPasswordGetterAndSetter() {
    final User gregoryPassword = new User();
    gregoryPassword.setPassword("123qwe");
    String gregoryPasswordSaved = gregoryPassword.getPassword();
    assertNotNull(gregoryPassword.getPassword());
    assertEquals(gregoryPasswordSaved, "123qwe");
  }

  /**
   * Test for user linkToAPI getter and setter
   */
  @Test
  public void testLinkToAPIGetterAndSetter() {
    final User shawnLinkToAPI = new User();
    shawnLinkToAPI.setLinkToAPI("http://1.1.1.1.com");
    String shawnLinkToAPISaved = shawnLinkToAPI.getLinkToAPI();
    assertNotNull(shawnLinkToAPI.getLinkToAPI());
    assertEquals(shawnLinkToAPISaved, "http://1.1.1.1.com");
  }


}