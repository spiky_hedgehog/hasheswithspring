package lt.viko.eif.SpringMongoHashService.controller;

import lt.viko.eif.SpringMongoHashService.domain.User;
import lt.viko.eif.SpringMongoHashService.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;
import java.util.List;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * User controller class for implementing the CRUD operations for users.
 */
@Configuration
@EnableSwagger2
@RestController
@RequestMapping("/users")
public class UserController {

  @Autowired
  private UserService userService;

  /**
   * Request method get that returns a list of all users
   *
   * @return the list of users
   */
  @RequestMapping(value = "/list/", method = RequestMethod.GET)
  public HttpEntity<List<User>> getAllUsers() {
    List<User> users = userService.findAll();
    if (users.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } else {
      users.forEach(e -> e
          .add(linkTo(methodOn(UserController.class).getAllUsers()).withRel("users")));
      users.forEach(e -> e.add(
          linkTo(methodOn(UserController.class).getUsersById(e.getUserId()))
              .withSelfRel()));
      return new ResponseEntity<>(users, HttpStatus.OK);
    }
  }

  /**
   * Request method get that returns a user based on his id
   *
   * @param userId id of the specific user we want to check
   * @return returns the user with that id
   */
  @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
  public HttpEntity<User> getUsersById(@PathVariable("id") String userId) {
    User byUserId = userService.findByUserId(userId);
    if (byUserId == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      byUserId.add(
          linkTo(methodOn(UserController.class).getUsersById(byUserId.getUserId()))
              .withSelfRel());
      return new ResponseEntity<>(byUserId, HttpStatus.OK);
    }
  }

  /**
   * Request method post for saving/creating a new user entry
   *
   * @param e object user
   * @return returns used id and password as a string value.
   */
  @RequestMapping(value = "/user/", method = RequestMethod.POST)
  public HttpEntity<?> saveUser(@RequestBody User e) {
    User user = userService.saveUser(e);
    URI location = ServletUriComponentsBuilder
        .fromCurrentRequest().path("/user/user/{id}")
        .buildAndExpand(user.getUserId()).toUri();
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setLocation(location);
    String response = e.getUserId() + " " + e.getPassword();
    return new ResponseEntity<>(response, HttpStatus.CREATED);

  }

  /**
   * Request method put used for updating the specific user
   *
   * @param id id of the user we want to update
   * @param e User object
   * @return returns the user that has been changed
   */
  @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
  public HttpEntity<?> updateUser(@PathVariable("id") String id, @RequestBody User e) {
    User byUserId = userService.findByUserId(id);
    if (byUserId == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      byUserId.setFirstName(e.getFirstName());
      byUserId.setLastName(e.getLastName());
      byUserId.setPassword(e.getPassword());
      userService.updateUser(byUserId);
      byUserId.add(
          linkTo(methodOn(UserController.class).getUsersById(byUserId.getUserId()))
              .withSelfRel());
      return new ResponseEntity<>(byUserId, HttpStatus.OK);
    }
  }

  /**
   * Request method delete used for deleting the specified user of the database
   *
   * @param userId id of the user we want to delete
   * @return response entity with no content
   */
  @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteUser(@PathVariable("id") String userId) {
    userService.deleteByUserId(userId);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  /**
   * Method used to delete all users of the database
   *
   * @return empty response entity
   */
  @RequestMapping(value = "/user/", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteAll() {
    userService.deleteAll();
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
