package lt.viko.eif.SpringMongoHashService.controller;

import lt.viko.eif.SpringMongoHashService.domain.HashFunctions;
import lt.viko.eif.SpringMongoHashService.domain.HashPairMatcher;
import lt.viko.eif.SpringMongoHashService.domain.IdentificationCreator;
import lt.viko.eif.SpringMongoHashService.domain.User;
import lt.viko.eif.SpringMongoHashService.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * HashController class used for getting user region and hashing
 */
@RestController
@RequestMapping("/hash")
public class HashController {

  @Autowired
  private UserService userService;

  /**
   * Generates hash depending on region
   *
   * @param e User class object
   * @return hash of a text
   */
  @RequestMapping(value = "/hashbylocation/", method = RequestMethod.POST)
  public String makeHash(@RequestBody User e) {

    String userId = e.getUserId();
    String password = e.getPassword();
    String linkToAPI = e.getLinkToAPI();
    String originalText = e.getText();

    String clientContinent = new IdentificationCreator().getClientRegion(linkToAPI);

    User byUserId = userService.findByUserId(userId);
    if (byUserId == null) {
      return "No such user";
    } else if (!byUserId.getPassword().equals(password)) {
      return "Wrong password";
    } else {
      return new HashPairMatcher().generateHash(originalText, clientContinent);
    }
  }

  /**
   * Generates hash with a salt
   *
   * @param otherUser User class object
   * @param salt manually entered by user
   * @return hash of a text
   */
  @RequestMapping(value = "/hashwithsalt/{salt}", method = RequestMethod.POST)
  public String saltedHash(@RequestBody User otherUser, @PathVariable("salt") String salt) {
    String userId = otherUser.getUserId();
    String password = otherUser.getPassword();
    String originalText = otherUser.getText();

    User byUserIdSecond = userService.findByUserId(userId);
    if (byUserIdSecond == null) {
      return "No such user";
    } else if (!byUserIdSecond.getPassword().equals(password)) {
      return "Wrong password";
    } else {
      return new HashFunctions().saltedHashWithCustomSalt(originalText, salt);
    }
  }


}
