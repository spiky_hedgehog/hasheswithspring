package lt.viko.eif.GatewaySpringMongoHashService.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import javax.servlet.http.HttpServletRequest;
import lt.viko.eif.GatewaySpringMongoHashService.domain.Client;
import lt.viko.eif.GatewaySpringMongoHashService.domain.Hash;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * HashingController class used for sending user plaintext and receiving hashed text
 */
@RestController
public class PlaintextSendingAndHashReceivingController {

  private HttpServletRequest request;

  @Autowired
  public void setRequest(HttpServletRequest request) {
    this.request = request;
  }

  /**
   * Request method POST for sending text and getting hash
   *
   * @param newClient object Client
   * @return returns hashed text as Hash object with links
   * @throws IOException needed for input stream
   */
  @RequestMapping(value = "/hashbylocation", method = RequestMethod.POST)
  public Hash hashedText(@RequestBody Client newClient)
      throws IOException {

    String clientID = newClient.getClientID();
    String clientPassword = newClient.getClientPassword();
    String originalText = newClient.getOriginalText();
    String IPAddress = calculateIP();

    URL url = new URL("http://service.stanul.com:8080/backend/hash/hashbylocation/");
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestMethod("POST");
    con.setRequestProperty("Content-Type", "application/json; utf-8");
    con.setRequestProperty("Accept", "application/json");
    con.setDoOutput(true);

    String jsonInputString =
        "{\"userId\": \"" + clientID + "\", \"password\": \"" + clientPassword
            + "\", \"linkToAPI\": \"" + IPAddress + "\", \"text\": \""
            + originalText + "\"}";

    try (OutputStream os = con.getOutputStream()) {
      byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
      os.write(input, 0, input.length);
    }

    try (BufferedReader br = new BufferedReader(
        new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
      StringBuilder response = new StringBuilder();
      String responseLine;
      while ((responseLine = br.readLine()) != null) {
        response.append(responseLine.trim());
      }
      Hash hash = new Hash(response.toString());

      Link link = ControllerLinkBuilder
          .linkTo(Hash.class)
          .slash("hashbylocation")
          .withSelfRel();

      Link linkToSalt = ControllerLinkBuilder
          .linkTo(Hash.class)
          .slash("hashwithsalt")
          .slash("{" + "salt" + "}")
          .withRel("hashwithsalt");

      hash.add(link);
      hash.add(linkToSalt);

      return hash;
    }
  }

  /**
   * @param newClient object for new client
   * @param salt salt value
   * @return returns hash with salt
   * @throws IOException needed for input stream
   */
  @RequestMapping(value = "/hashwithsalt/{salt}", method = RequestMethod.POST)
  public Hash hashedTextSalt(@RequestBody Client newClient, @PathVariable("salt") String salt)
      throws IOException {

    String clientID = newClient.getClientID();
    String clientPassword = newClient.getClientPassword();
    String originalText = newClient.getOriginalText();

    URL url = new URL("http://service.stanul.com:8080/backend/hash/hashwithsalt/" + salt);
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestMethod("POST");
    con.setRequestProperty("Content-Type", "application/json; utf-8");
    con.setRequestProperty("Accept", "application/json");
    con.setDoOutput(true);

    String jsonInputString =
        "{\"userId\": \"" + clientID + "\", \"password\": \"" + clientPassword
            + "\", \"text\": \""
            + originalText + "\"}";

    try (OutputStream os = con.getOutputStream()) {
      byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
      os.write(input, 0, input.length);
    }

    try (BufferedReader br = new BufferedReader(
        new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
      StringBuilder response = new StringBuilder();
      String responseLine;
      while ((responseLine = br.readLine()) != null) {
        response.append(responseLine.trim());
      }
      Hash hash = new Hash(response.toString());

      Link link = ControllerLinkBuilder
          .linkTo(Hash.class)
          .slash("hashwithsalt")
          .slash(salt)
          .withSelfRel();

      Link linkToDefault = ControllerLinkBuilder
          .linkTo(Hash.class)
          .slash("hashbylocation")
          .withRel("hashbylocation");

      hash.add(link);
      hash.add(linkToDefault);

      return hash;
    }
  }


  /**
   * Method to find out IP address of client who sent out request
   *
   * @return IP address
   */
  private String calculateIP() {
    String ip = request.getHeader("X-Forwarded-For");
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("WL-Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("HTTP_CLIENT_IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("HTTP_X_FORWARDED_FOR");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getRemoteAddr();
    }
    return "http://ip-api.com/json/" + ip + "?fields=countryCode";
  }


}
