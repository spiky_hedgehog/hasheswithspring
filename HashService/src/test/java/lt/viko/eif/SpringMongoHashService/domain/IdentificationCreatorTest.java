package lt.viko.eif.SpringMongoHashService.domain;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

public class IdentificationCreatorTest {

  /**
   * Test of Location extraction from IP
   */
  @Test
  public void getClientRegion() {
    RestTemplate restTemplate = new RestTemplate();
    String countryCode = restTemplate.getForObject("http://ip-api.com/json/138.199.22.171?fields=countryCode", String.class).substring(16, 18);
    assertEquals(countryCode, "JP");
  }

}
