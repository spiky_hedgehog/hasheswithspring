package lt.viko.eif.GatewaySpringMongoHashService.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import lt.viko.eif.GatewaySpringMongoHashService.domain.Client;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Registration controller class for implementing new user registration.
 */
@RestController
public class RegistrationController {

  /**
   * Request method post for registering new user
   *
   * @param newClient object User
   * @return returns client object with links
   * @throws IOException needed for input stream
   */
  @RequestMapping(value = "/register", method = RequestMethod.POST)
  public Client registerAccount(@RequestBody Client newClient)
      throws IOException {

    String firstName = newClient.getFirstName();
    String lastName = newClient.getLastName();

    URL url = new URL("http://service.stanul.com:8080/backend/users/user/");
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestMethod("POST");
    con.setRequestProperty("Content-Type", "application/json; utf-8");
    con.setRequestProperty("Accept", "application/json");
    con.setDoOutput(true);

    String jsonInputString =
        "{\"firstName\": \"" + firstName + "\", \"lastName\": \"" + lastName + "\"}";

    try (OutputStream os = con.getOutputStream()) {
      byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
      os.write(input, 0, input.length);
    }

    try (BufferedReader br = new BufferedReader(
        new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
      StringBuilder response = new StringBuilder();
      String responseLine;
      while ((responseLine = br.readLine()) != null) {
        response.append(responseLine.trim());
      }
      String[] information = response.toString().split(" ");
      newClient.setClientID(information[0]);
      newClient.setClientPassword(information[1]);

      Link link = ControllerLinkBuilder
          .linkTo(Client.class)
          .slash("register")
          .withSelfRel();

      Link OtherMethodLink = ControllerLinkBuilder
          .linkTo(Client.class)
          .slash("hashservice")
          .withRel("ServiceLink");

      newClient.add(link);
      newClient.add(OtherMethodLink);
    }
    return newClient;

  }

}
