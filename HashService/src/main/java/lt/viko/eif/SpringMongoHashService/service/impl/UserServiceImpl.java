package lt.viko.eif.SpringMongoHashService.service.impl;

import lt.viko.eif.SpringMongoHashService.domain.User;
import lt.viko.eif.SpringMongoHashService.repository.UserRepository;
import lt.viko.eif.SpringMongoHashService.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Class that implements the user service interface
 */
@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepository;

  /**
   * Implementation of save user
   */
  @Override
  public User saveUser(User e) {
    return userRepository.save(e);
  }

  /**
   * Implementation of findByUserID
   */
  @Override
  public User findByUserId(String userId) {
    return userRepository.findOne(userId);
  }

  /**
   * Implementation of deleteByUserId
   */
  @Override
  public void deleteByUserId(String userId) {
    userRepository.delete(userId);
  }

  /**
   * Implementation of updateUser
   */
  @Override
  public void updateUser(User e) {
    userRepository.save(e);
  }

  /**
   * Implementation of userExists
   */
  @Override
  public boolean userExists(User e) {
    return userRepository.exists(Example.of(e));
  }

  /**
   * Implementation of findAll method
   */
  @Override
  public List<User> findAll() {
    return userRepository.findAll();
  }

  /**
   * Implementation of deleteAll method
   */
  @Override
  public void deleteAll() {
    userRepository.deleteAll();
  }
}
