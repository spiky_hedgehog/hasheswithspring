package lt.viko.eif.SpringMongoHashService.service;

import lt.viko.eif.SpringMongoHashService.domain.User;
import java.util.List;

/**
 * UserService interface
 */
public interface UserService {

  /**
   * Method for saving the user
   *
   * @param e object user
   * @return returns used id and password as a string value.
   */
  User saveUser(User e);


  /**
   * @param userId is used to find ser
   * @return returns found user
   */
  User findByUserId(String userId);

  /**
   * Method for deleting the user
   *
   * @param userId user ID who's gonna be deleted
   */
  void deleteByUserId(String userId);

  /**
   * Method for updating the user
   *
   * @param e user object
   */
  void updateUser(User e);

  /**
   * Method for checking if user exists
   *
   * @param e user object
   * @return returns boolean value if user exists true else false
   */
  boolean userExists(User e);

  /**
   * Method for finding all users
   *
   * @return returns list of users
   */
  List<User> findAll();

  /**
   * Method for deleting all users from database
   */
  void deleteAll();
}
