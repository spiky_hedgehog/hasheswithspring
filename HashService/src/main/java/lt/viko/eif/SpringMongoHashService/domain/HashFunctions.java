package lt.viko.eif.SpringMongoHashService.domain;

import java.nio.charset.StandardCharsets;
import com.google.common.hash.Hashing;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * import java.security.MessageDigest;
 *
 * Class used for storing the hash functions used for the service
 */
public class HashFunctions {

  public HashFunctions() {
  }

  /**
   * Method that converts text to md5 hash
   *
   * @param text string of text we want to hash
   * @return the string value of md5 hash
   */
  String md5(String text) {
    return Hashing.md5()
        .hashString(text, StandardCharsets.UTF_8)
        .toString().toUpperCase();
  }

  /**
   * Method that converts text to sha256 hash
   *
   * @param text string of text we want to hash
   * @return the string value of sha256 hash
   */
  String sha256(String text) {
    return Hashing.sha256()
        .hashString(text, StandardCharsets.UTF_8)
        .toString().toUpperCase();
  }

  /**
   * Method that converts text to sha384 hash
   *
   * @param text string of text we want to hash
   * @return the string value of sha384 hash
   */
  String sha384(String text) {
    return Hashing.sha384()
        .hashString(text, StandardCharsets.UTF_8)
        .toString().toUpperCase();
  }

  /**
   * Method that converts text to sha512 hash
   *
   * @param text string of text we want to hash
   * @return the string value of sha512 hash
   */
  String sha512(String text) {
    return Hashing.sha512()
        .hashString(text, StandardCharsets.UTF_8)
        .toString().toUpperCase();
  }

  /**
   * Method that converts text to murmur3_32 hash
   *
   * @param text string of text we want to hash
   * @return the string value of murmur3_32 hash
   */
  String murmur3_32(String text) {
    return Hashing.murmur3_32()
        .hashString(text, StandardCharsets.UTF_8)
        .toString().toUpperCase();
  }

  /**
   * Method that converts text to murmur3_128 hash
   *
   * @param text string of text we want to hash
   * @return the string value of murmur3_128 hash
   */
  String murmur3_128(String text) {
    return Hashing.murmur3_128()
        .hashString(text, StandardCharsets.UTF_8)
        .toString().toUpperCase();
  }

  /**
   * Method that converts text to sipHash24 hash
   *
   * @param text string of text we want to hash
   * @return the string value of sipHash24 hash
   */
  String sipHash24(String text) {
    return Hashing.sipHash24()
        .hashString(text, StandardCharsets.UTF_8)
        .toString().toUpperCase();
  }


  /**
   * Method that generates hash using salt provided by user
   *
   * @param ogText original text received from user
   * @param saltString salt received from user
   * @return the string value of generated hash
   */
  public String saltedHashWithCustomSalt(String ogText, String saltString) {
    byte[] salt = saltString.getBytes();

    String generatedHashedText = null;
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      md.update(salt);
      md.update(ogText.getBytes());
      byte[] bytes = md.digest();
      StringBuilder sb = new StringBuilder();
      for (byte aByte : bytes) {
        sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
      }
      generatedHashedText = sb.toString();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return (generatedHashedText);
  }


}
