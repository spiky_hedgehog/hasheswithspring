package lt.viko.eif.GatewaySpringMongoHashService.domain;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Class containing methods for testing Hash class getters and setters
 */
public class HashTest {

  /**
   * Test for Hash hash value getter and setter
   */
  @Test
  public void testHashGetterAndSetter() {
    final Hash hash = new Hash();
    hash.setHash("1b279d2734eb04ab0937ec7c72f8cd37");
    String getHash = hash.getHash();
    assertNotNull(hash.getHash());
    assertEquals(getHash, "1b279d2734eb04ab0937ec7c72f8cd37");
  }

}