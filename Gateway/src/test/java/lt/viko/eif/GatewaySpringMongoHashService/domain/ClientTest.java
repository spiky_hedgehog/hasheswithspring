package lt.viko.eif.GatewaySpringMongoHashService.domain;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Class containing methods for testing Client class getters and setters
 */
public class ClientTest {

  /**
   * Test for Client first name getter and setter
   */
  @Test
  public void testFirstNameGetterAndSetter() {
    final Client bobName = new Client();
    bobName.setFirstName("Bob");
    String bobNameSaved = bobName.getFirstName();
    assertNotNull(bobName.getFirstName());
    assertEquals(bobNameSaved, "Bob");
  }

  /**
   * Test for Client last name getter and setter
   */
  @Test
  public void testLastNameGetterAndSetter() {
    final Client aliceName = new Client();
    aliceName.setLastName("Wax");
    String aliceNameSaved = aliceName.getLastName();
    assertNotNull(aliceName.getLastName());
    assertEquals(aliceNameSaved, "Wax");
  }

  /**
   * Test for Client ID getter and setter
   */
  @Test
  public void testClientIDGetterAndSetter() {
    final Client badgerClientID = new Client();
    badgerClientID.setClientID("Nr12");
    String badgerClientIDSaved = badgerClientID.getClientID();
    assertNotNull(badgerClientID.getClientID());
    assertEquals(badgerClientIDSaved, "Nr12");
  }

  /**
   * Test for Client password getter and setter
   */
  @Test
  public void testClientPasswordGetterAndSetter() {
    final Client cowPassword = new Client();
    cowPassword.setClientPassword("qwe123");
    String cowPasswordSaved = cowPassword.getClientPassword();
    assertNotNull(cowPassword.getClientPassword());
    assertEquals(cowPasswordSaved, "qwe123");
  }

  /**
   * Test for Client original text getter and setter
   */
  @Test
  public void testOriginalTextGetterAndSetter() {
    final Client sparrowOriginalText = new Client();
    sparrowOriginalText.setOriginalText("Bird sounds");
    String sparrowOriginalTextSaved = sparrowOriginalText.getOriginalText();
    assertNotNull(sparrowOriginalText.getOriginalText());
    assertEquals(sparrowOriginalTextSaved, "Bird sounds");
  }


}