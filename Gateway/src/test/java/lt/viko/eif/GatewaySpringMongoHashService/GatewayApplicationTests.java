package lt.viko.eif.GatewaySpringMongoHashService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Class for client side application test
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GatewayApplicationTests {

  /**
   * Test for context Load
   */
  @Test
  public void contextLoads() {
  }

}
