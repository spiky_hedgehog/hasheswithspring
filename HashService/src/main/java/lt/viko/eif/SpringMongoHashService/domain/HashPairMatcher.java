package lt.viko.eif.SpringMongoHashService.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Class used for matching Hash with the specific continent
 */
public class HashPairMatcher {

  /**
   * Map String String where one string is region and other is hash value
   */
  private static Map<String, String> map = new HashMap<>();


  /**
   * Method for hashing the specific region with specific hash
   *
   * @param originalString original text we want to hash
   */
  private void HashNames(String originalString) {
    map.put("Africa", new HashFunctions().md5(originalString));
    map.put("Antarctica", new HashFunctions().murmur3_32(originalString));
    map.put("Asia", new HashFunctions().murmur3_128(originalString));
    map.put("Australia", new HashFunctions().sha256(originalString));
    map.put("Europe", new HashFunctions().sha384(originalString));
    map.put("North America", new HashFunctions().sha512(originalString));
    map.put("South America", new HashFunctions().sipHash24(originalString));
  }

  /**
   * @param originalText text received from gateway
   * @param Region geographical region of a client
   * @return hash of a originalText
   */
  public String generateHash(String originalText, String Region) {
    HashNames(originalText);
    String hashedText = map.get(Region);
    map.clear();
    if (hashedText == null) {
      hashedText = "Hash broke";
    }
    return hashedText;
  }

}


